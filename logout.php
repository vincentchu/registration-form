<?php 
    date_default_timezone_set("America/Chicago");
    include_once 'resources/session.php';

    session_destroy();
    header('location: index.php');
?>
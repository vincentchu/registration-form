<?php
    date_default_timezone_set("America/Chicago");
    include_once 'resources/Database.php';
	require 'resources/mailer/PHPMailerAutoload.php';

    $data = array();

    if (isset($_POST['email'])) {
        $school = $_POST['school'];
        $email = $_POST['email'];
        $display = $_POST['display'];
        $password = $_POST['password'];
        $password2 = $_POST['password2'];
        $account = $_POST['accountType'];
        
        if($password != $password2) {
            $data['Success'] = false;
            $data['Message'] = "Passwords do not match.";
            exit(json_encode($data));
        } elseif (strlen($password ) < 8){
            $data['Success'] = false;
            $data['Message'] = "Password is too short.";
            exit(json_encode($data));
        } else {
            $hashed_password = password_hash($password, PASSWORD_ARGON2I);
        }
        
        try{
            $sqlInsert = "INSERT INTO users (displayname, password, email, school, account_type) VALUES (:display, :password, :email, :school, :account)";

            $statement = $db->prepare($sqlInsert);
            $statement->execute(array(':display' => $display, ':password' => $hashed_password, ':email' => $email, ':school' => $school, ':account' => $account));

            if($statement->rowCount() == 1) {
                $data['Success'] = true;
				$data['email'] = $email;
				$mail = new PHPMailer;
				$mail->isSMTP();                                      
				$mail->Host = 'smtp.sparkpostmail.com';  
				$mail->SMTPAuth = true;                               
				$mail->Username = 'SMTP_Injection';                 
				$mail->Password = '2dda99e53d5ff543e985718d55a76091a9e9e3d6';   $mail->SMTPSecure = 'starttls';                     
				$mail->Port = 587;                          
				$mail->setFrom('mx@mail.quickbrownfoxes.org', 'no-reply');
				$mail->addAddress($email);
				$mail->isHTML(true);                                  
				$mail->Subject = 'Confirm Registration - Arpeggio App';
				$mail->Body    = '<!DOCTYPE html><head><meta charset="UTF-8"><title>Welcome to Arpeggio</title><style type="text/css">@media only screen and (max-width: 480px){table{display: block !important;width: 100% !important;}td{width: 480px !important;}}</style></head><body style="font-family: \'Malgun Gothic\', Arial, sans-serif; margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none; -webkit-font-smoothing: antialiased;"><table width="100%" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" id="background" style="height: 100% !important; margin: 0; padding: 0; width: 100% !important;"><tr><td align="center" valign="top"><table width="600" border="0" bgcolor="#F6F6F6" cellspacing="0" cellpadding="20" id="preheader"><tr><td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td valign="top" width="600"><div class="preheader_links"><p style="color: #666666; font-size: 10px; line-height: 22px; text-align: right;">Unable to view this message? <a href="" style="color: #666666; font-weight: bold; text-decoration: none;">Click here</a></p></div></td></tr></table></td></tr></table><table width="600" border="0" cellspacing="0" cellpadding="20" id="body_info_container"><tr><td align="center" valign="top" class="body_info_content"><table width="100%" border="0" cellspacing="0" cellpadding="20"><tr><td valign="top"><h1 style="color: #222; font-size: 25px; text-align: center;">Welcome to Arpeggio!</h1><p style="color: #666666; font-size: 14px; line-height: 22px; text-align: left;">One last step. Click on the button below to verify you email and finish creating your account</p><tr><td align="center"><div><a href="" style="background-color:#00C851;border-radius:3px;color:#FFFFFF;display:inline-block;font-family:\'Helvetica\',Arial,sans-serif;font-size:13px;height:45px;line-height:45px;text-align:center;text-decoration:none;text-transform:uppercase;width:140px;-webkit-text-size-adjust:none;mso-hide:all;">Verify Email</a><p style="color: #666666; font-size: 12px; line-height: 22px; text-align: center;"> Link not working? https://arpeggioapp.org/signup/NzU3Nzg3Njc3NzktZGY1OTcyYmEwYw</p></div></td></tr></td></tr></table></td></tr></table></td></tr></table></body></html>';
				$mail->AltBody    = '' . $email . '\n' . 'Follow this link to confirm your account' . '\n' . 'https://arpeggioapp.org/signup/NzU3Nzg3Njc3NzktZGY1OTcyYmEwYw'; 
				
				if(!$mail->send()) {
					header('Location: https://arpeggioapp.org/auth/signup/error');
				} else {
					$data['Message'] = "Registration Successful! Please check your email for the verification link.";
				}
            }
        } catch (PDOException $ex) {
            $data['Success'] = false;
            if(strpos($ex->getMessage(), "for key 'email'") !== false) {
                $data['Message'] = "An account with that email address already exists.";
            } elseif (strpos($ex->getMessage(), "for key 'display'") !== false) {
                $data['Message'] = "Display name not available.";
            } else {
                $data['Message'] = "An Error occured during registration." . $ex->getMessage();
            }
        }
        
        echo json_encode($data);
    }
?>

<?php
require 'resources/mailer/PHPMailerAutoload.php';
$errors = array();
$data = array();

if(empty($_POST['email'])) $errors['email'] = 'Email is required!';

if (!empty($errors)) {
	$data['success'] = false;
	$data['errors'] = $errors;
	echo json_encode($data);
	exit;
} else {
	$data['success'] = true;
	$data['info'] = 'Message Sent!';
}

$email = $_POST['email'];
$data['email'] = $email;
$mail = new PHPMailer;
$mail->isSMTP();                                      
$mail->Host = 'smtp.sparkpostmail.com';  
$mail->SMTPAuth = true;                               
$mail->Username = 'SMTP_Injection';                 
$mail->Password = '2dda99e53d5ff543e985718d55a76091a9e9e3d6';   $mail->SMTPSecure = 'starttls';                     
$mail->Port = 587;                          
$mail->setFrom('mx@mail.quickbrownfoxes.org', 'no-reply');
$mail->addAddress($email);
$mail->isHTML(true);                          

$mail->Subject = 'Forgot Password? Arpeggio App';
$mail->Body    = '<!DOCTYPE html><head> <meta charset="UTF-8"> <title>Forgot Password</title></head><body> <style type="text/css"> @media only screen and (max-width: 480px){table{display: block !important; width: 100% !important;}td{width: 480px !important;}}</style> <body style="font-family: \'Malgun Gothic\', Arial, sans-serif; margin: 0; padding: 0; width: 100%; -webkit-text-size-adjust: none; -webkit-font-smoothing: antialiased;"> <table width="100%" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" id="background" style="height: 100% !important; margin: 0; padding: 0; width: 100% !important;"> <tr> <td align="center" valign="top"> <table width="600" border="0" bgcolor="#F6F6F6" cellspacing="0" cellpadding="20" id="preheader"> <tr> <td valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0"> <tr> <td valign="top" width="600"> <div class="preheader_links"> <p style="color: #666666; font-size: 10px; line-height: 22px; text-align: right;">Unable to view this message? <a href="" style="color: #666666; font-weight: bold; text-decoration: none;">Click here</a></p></div></td></tr></table> </td></tr></table> <table width="600" border="0" cellspacing="0" cellpadding="20" id="body_info_container"> <tr> <td align="center" valign="top" class="body_info_content"> <table width="100%" border="0" cellspacing="0" cellpadding="20"> <tr> <td valign="top"> <h2 style="color: #222; font-size: 20px; text-align: center;">Forgot Password?</h2> <p style="color: #666666; font-size: 14px; line-height: 22px; text-align: left;">Click the below button to reset your password for Arpeggio. The link will be valid for 24 hours. If you did not make this request, you may safely ignore and discard this email.</p><tr> <td align="center"> <div> <a href="#" style="background-color:#0099CC;border-radius:3px;color:#FFFFFF;display:inline-block;font-family:\'Helvetica\',Arial,sans-serif;font-size:13px;height:45px;line-height:45px;text-align:center;text-decoration:none;text-transform:uppercase;width:170px;-webkit-text-size-adjust:none;mso-hide:all;">Reset Password</a> <p style="color: #666666; font-size: 12px; line-height: 22px; text-align: center;"> Link not working? https://arpeggioapp.org/signup/NzU3Nzg3Njc3NzktZGY1OTcyYmEwYw</p></div></td></tr></td></tr></table> </td></tr></table> </td></tr></table> </body></html>';
$mail->AltBody    = '' . $email . '\n' . 'Follow this link to reset your password' . '\n' . 'https://arpeggioapp.org/signup/NzU3Nzg3Njc3NzktZGY1OTcyYmEwYw'; 
if(!$mail->send()) {
    header('Location: https://arpeggioapp.org/auth/forgot/error');
} else {
    header('Location: https://arpeggioapp.org/auth/forgot/success');
}

?>






<?php
    date_default_timezone_set("America/Chicago");
    include_once 'resources/Database.php';
    include_once 'resources/utilities.php';
    session_start();

    if(isset($_POST['login'])) {

        $email = $_POST['email'];
        $password = $_POST['password'];

        $sqlQuery = "SELECT * FROM users WHERE email = :email";
        $statement = $db->prepare($sqlQuery);
        $statement->execute(array(':email' => $email));

        if($row = $statement->fetch()){
            $id = $row['id'];
            $hashed_password = $row['password'];
            $display = $row['displayname'];
            $email = $row['email'];
            $joined = $row['join_date'];
            $school = $row['school'];
            $account = $row['account_type'];

            if(password_verify($password, $hashed_password)){
                $_SESSION['id'] = $id;
                $_SESSION['displayname'] = $display;
                $_SESSION['school'] = $school;
                $_SESSION['email'] = $email;
                $_SESSION['joined'] = $joined;
                $_SESSION['account_type'] = $account;
                $success = true;
				header("Location: ../dashboard");
				die();
            } else {
                $error = true;
                $message = "Invalid Email or Password.";
            }
            
        } else {
            $error = true;
            $message = "Email not registered.";
        }
        
    }
?>
	<!DOCTYPE html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js" integrity="sha256-3SrLjPeRPa1ofM280r+OMcUjJZKLWJHr6SRtRu3dRb0=" crossorigin="anonymous"></script>
		<script src="../resources/js/arpeggio.js"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700">

		<title>Arpeggio &raquo; Home</title>

		<link href="../resources/css/style.css" rel="stylesheet">

		<link rel="icon" href="../resources/images/favicon.ico">

	</head>

	<body class="login" ng-app="arpeggioApp">
		<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navcollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
			<a class="navbar-brand" href="../">Arpeggio</a>

			<div class="collapse navbar-collapse" id="navbar">
				<ul class="navbar-nav mr-auto">
					<br />
				</ul>
				<div class="my-2 my-lg-0">
					<li class="nav-item"><a href="#">Log-In / Register</a></li>
				</div>
			</div>
		</nav>


		<div class="login-wrap mx-auto">
			<div class="login-html">
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
				<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Sign Up</label>
				<div class="login-form">
					<div class="sign-in-htm">
						<form action="index.php" method="post">
							<div class="group">
								<label for="user" class="label">Email Address</label>
								<input type="text" class="input" value="" name="email" required>
							</div>
							<div class="group">
								<label for="pass" class="label">Password</label>
								<input type="password" class="input" value="" name="password" data-type="password" required>
							</div>
							<div class="group">
								<input id="check" type="checkbox" class="check" checked>
								<label for="check"><span class="icon"></span> Keep me Signed in</label>
							</div>
							<?php if(isset($success)) { echo '<div id="loginMessage" class="alert alert-success" >' . $message . '</div>';}?>
							<?php if(isset($error)) { echo '<div id="loginError" class="alert alert-danger" >' . $message . '</div>';}?>
							<?php if(!isset($success)) { echo '<div class="group">
								<input type="submit" class="button" name="login" value="Log In">
							</div>';}?>
							<hr />
						</form>

						<hr />

						<div class="foot-lnk">
							<form action="forgot.php" method="post">
								<a data-toggle="collapse" href="#forgotpass" aria-expanded="false" aria-controls="forgotpass">
									Forgot Password?
								</a>
								<div class="collapse" id="forgotpass">
									<br />
									<div class="group">
										<label for="user" class="label">Enter the e-mail associated with your account, then click Submit. We'll send you a link to where you can create a new password.</label>
										<input type="text" class="input" value="" name="email" required>
									</div>
									<div class="g-recaptcha" data-sitekey="6LcQMiEUAAAAAKdKL-49zPwwrkAboeyQIP_qAcU8">
									</div>
									<br />
									<div class="group">
										<input type="submit" class="btn btn-secondary btn-md btn-block float-md-right" value="Submit">
									</div>
								</div>
							</form>
						</div>

					</div>

					<div class="sign-up-htm" ng-controller="RegisterController">
						<form name="registerForm" ng-submit="register(registerForm.$valid)" novalidate>
							<div id="registerMessage" class="alert alert-success" ng-show="registerSuccess">{{registerMessage}}</div>
							<div id="registerError" class="alert alert-danger" ng-show="registerError">{{registerMessage}}</div>
							<div class="group">
								<label for="user" class="label">Display Name&nbsp;<span ng-show="registerForm.display.$invalid && !registerForm.display.$pristine && !registerForm.display.$error.maxlength" class="help-block">- Must be at least 5 characters.</span><span ng-show="registerForm.display.$invalid && !registerForm.display.$pristine && registerForm.display.$error.maxlength" class="help-block">- Must be less than 35 characters.</span></label>
								<input type="text" name="display" class="input" ng-model="newUser.display" ng-minlength="5" ng-maxlength="35" required>
							</div>
							<div class="group">
								<label for="email" class="label">Email Address&nbsp;<span ng-show="registerForm.email.$invalid && !registerForm.email.$pristine && registerForm.email.$error.maxlength" class="help-block">- Must be less than 255 characters.</span><span ng-show="registerForm.email.$invalid && !registerForm.email.$pristine && !registerForm.email.$error.maxlength" class="help-block">- Enter a valid email.</span></label>
								<input type="email" name="email" class="input" ng-model="newUser.email" required>
							</div>
							<div class="group">
								<label for="pass" class="label">Password&nbsp;<span ng-show="registerForm.password.$error.minlength" class="help-block">- Must be at least 8 characters.</span><span ng-show="registerForm.password.$error.maxlength" class="help-block">- Password is too long.</span></label>
								<input type="password" name="password" class="input" ng-model="newUser.password" ng-minlength="8" ng-maxlength="255" data-type="password" required>
							</div>
							<div class="group">
								<label for="pass" class="label">Repeat Password</label>
								<input type="password" name="password2" class="input" ng-model="newUser.password2" ng-maxlength="255" data-type="password" required>
							</div>
							<div class="group">
								<label for="exampleSelect1">School:</label>
								<br />
								<select class="form-control" ng-model="newUser.school" required>
                                            <option value="None">None</option>
                                            <option value="Auburn University">Auburn University</option>
                                            <option value="Other">Other</option>
                                        </select>
							</div>
							<label>Account Type:</label>
							<br />
							<div class="btn-group" data-toggle="buttons" style="margin-bottom:35px">
								<label class="btn btn-success active" ng-click="setAccount('Student')">
                                                    <input type="radio" autocomplete="off" value="Student" checked> Student
                                                </label>
								<label class="btn btn-success" ng-click="setAccount('Teacher')">
                                                    <input type="radio" autocomplete="off" value="Teacher" > Teacher
                                                </label>
								<label class="btn btn-success" ng-click="setAccount('Neither')">
                                                    <input type="radio" autocomplete="off" value="Neither"> Neither
                                                </label>
							</div>
							<div class="group">
								<input type="submit" ng-disabled="registerForm.$invalid" onclick="lockoutSubmit(this)" class="float-md-right button btn btn-primary btn-lg" value="Sign Up!">
							</div>
							<br />
						</form>
					</div>
				</div>
			</div>
		</div>








		<script async src='https://www.google.com/recaptcha/api.js'></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		<script async src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha256-+kIbbrvS+0dNOjhmQJzmwe/RILR/8lb/+4+PUNVW09k=" crossorigin="anonymous"></script>
		<script async src="../resources/js/ie10-viewport-bug-workaround.js"></script>
		<script>
			function lockoutSubmit(button) {
				var oldValue = button.value;

				button.setAttribute('disabled', true);
				button.value = 'Please wait...';

				setTimeout(function() {
					button.value = oldValue;
					button.removeAttribute('disabled');
				}, 3000)
			}

		</script>
	</body>

	</html>
